AR-qualys
=========

Tasks that either reference the qualys api, or aid in the deployment and configuration of the qualys agents.
Qualys is the closest thing we have to a gold-standard inventory. The playbooks leverage these circumstances, use your judgement when a better source develops.

Requirements
------------

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
